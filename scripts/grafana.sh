#!/bin/sh
kubectl get secret \
    --namespace flux-system loki-chart-grafana \
    -o jsonpath="{.data.admin-password}" | \
    base64 --decode ; echo