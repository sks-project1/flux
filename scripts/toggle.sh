#!/usr/bin/env bash

create() {
    if [ $# -lt 1 ]; then
        echo "Error: Insufficient arguments. Usage: $0 create <toggle_name>"
        exit 1
    fi
    adminkey=$(kubectl get secret apikeys-unleash -n unleash -o jsonpath='{.data.admin}' | base64 -d)
    toggle=$1
    kubectl run temp-curl-pod -n unleash --restart=Never --image=curlimages/curl -- /bin/sh -c \
        "curl --location --request POST 'http://unleash-server.unleash:4242/api/admin/projects/default/features' \
        --header 'Authorization: $adminkey' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            \"type\": \"release\",
            \"name\": \"$toggle\",
            \"description\": \"First Toggle\",
            \"impressionData\": false
        }'"
    kubectl delete pod temp-curl-pod -n unleash
}

toggleOff() {
    if [ $# -lt 1 ]; then
        echo "Error: Insufficient arguments. Usage: $0 create <toggle_name>"
        exit 1
    fi
    togglename=$1
    adminkey=$(kubectl get secret apikeys-unleash -n unleash -o jsonpath='{.data.admin}' | base64 -d)
    kubectl run temp-curl-pod -n unleash --restart=Never --image=curlimages/curl -- /bin/sh -c \
        "curl -L -X POST 'http://unleash-server.unleash:4242/api/admin/projects/default/features/$togglename/environments/development/off' \
        -H 'Accept: application/json' \
        -H 'Authorization: $adminkey';
        curl -L -X POST 'http://unleash-server.unleash:4242/api/admin/projects/default/features/$togglename/environments/production/off' \
        -H 'Accept: application/json' \
        -H 'Authorization: $adminkey';"
    kubectl delete pod temp-curl-pod -n unleash
}

toggleOn() {
    if [ $# -lt 1 ]; then
        echo "Error: Insufficient arguments. Usage: $0 create <toggle_name>"
        exit 1
    fi
    togglename=$1
    adminkey=$(kubectl get secret apikeys-unleash -n unleash -o jsonpath='{.data.admin}' | base64 -d)
    kubectl run temp-curl-pod -n unleash --restart=Never --image=curlimages/curl -- /bin/sh -c \
        "curl --location --request POST 'http://unleash-server.unleash:4242/api/admin/projects/default/features/$togglename/environments/development/on' \
        --header 'Authorization: $adminkey' \
        --header 'Content-Type: application/json';
        curl --location --request POST 'http://unleash-server.unleash:4242/api/admin/projects/default/features/$togglename/environments/production/on' \
        --header 'Authorization: $adminkey' \
        --header 'Content-Type: application/json';"
    kubectl delete pod temp-curl-pod -n unleash
}

case "$1" in
    create | c)
        create $2        
        ;;
    toggleOn | on)
        toggleOn $2
        ;;
    toggleOff | off)
        toggleOff $2
        ;;
    *)
        echo "Usage: $0 {(create|c)|(toggleOn|on)|(toggleOff|off)}"
        exit 1
        ;;
esac
