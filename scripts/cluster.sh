#!/usr/bin/env bash

CLUSTER_NAME=$2

if [ -z "$CLUSTER_NAME" ]; then
    CLUSTER_NAME="dev"
fi

echo "Cluster name: kind-${CLUSTER_NAME}"

create() {
    kind create cluster \
        --name $CLUSTER_NAME \
        --config kind-config.yaml
    kubectl cluster-info --context "kind-${CLUSTER_NAME}"
    kubectl apply -f secrets/apikeys-unleash.yaml
}

bootstrap() {
    # https://fluxcd.io/flux/installation/bootstrap/gitlab/
    export GITLAB_TOKEN=$(cat secrets/token-fluxcd.key)
    flux check --pre
    flux bootstrap gitlab \
        --deploy-token-auth \
        --owner=sks-project1 \
        --repository=flux \
        --branch=main \
        --path=cluster \
        --personal
    flux check
}

delete() {
    kind delete cluster --name $CLUSTER_NAME
}

case "$1" in
    create | c)
        create        
        ;;
    delete | d)
        delete
        ;;
    bootstrap | b)
        bootstrap
        ;;
    *)
        echo "Usage: $0 {(create|c)|(delete|d)|(bootstrap|b)}"
        exit 1
        ;;
esac
