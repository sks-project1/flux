#!/usr/bin/env bash

FOLDER_NAME="secrets"

# expect age

if ! command -v age &> /dev/null
then
    echo "age file encryption could not be found"
    exit
fi

encrypt() {
    echo "Encrypting secrets..."
    secrets=$(find $FOLDER_NAME -type f -not -name "*.age" -not -path "*.git*" -not -path "*.pub")
    for secret in $secrets
    do
      age -p --armor -o "${secret}.age" "${secret}" 
      rm "${secret}" 
    done
}

decrypt() {
    echo "Decrypting secrets..."
    secrets=$(find $FOLDER_NAME -type f -name "*.age") 
    for secret in $secrets
    do
      age -d -o "${secret%.*}" "${secret}"
    done
}

case "$1" in
    encrypt | e)
        encrypt
        ;;
    decrypt | d)
        decrypt
        ;;
    *)
        echo "Usage: $0 {(encrypt|e)|(decrypt|d)}"
        exit 1
        ;;
esac