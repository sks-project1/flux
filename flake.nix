{
  description = "A Nix-flake-based Java development environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = { self, nixpkgs, pre-commit-hooks }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        config.allowUnfree = true;
        inherit system;
      };
      checks = {
        pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            nixpkgs-fmt.enable = true;
          };
        };
      };
    in
    {
      devShells.${system}.default = pkgs.mkShell {
        packages = with pkgs; [
          age
          fluxcd
          gitlab-runner
          k9s
          kind
          kubectx
          kubernetes
          kubernetes-helm
          pre-commit
          (
            vscode-with-extensions.override {
              vscodeExtensions = with vscode-extensions; [
                christian-kohler.path-intellisense
                gitlab.gitlab-workflow
                github.copilot
                redhat.java
                redhat.vscode-xml
                redhat.vscode-yaml
                vscjava.vscode-java-debug
                vscjava.vscode-java-test
                vscjava.vscode-maven
                jnoortheen.nix-ide
              ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
                {
                  name = "helm-intellisense";
                  publisher = "tim-koehler";
                  version = "0.14.3";
                  sha256 = "sha256-TcXn8n6mKEFpnP8dyv+nXBjsyfUfJNgdL9iSZwA5eo0=";
                }
                {
                  name = "vscode-gitops-tools";
                  publisher = "Weaveworks";
                  version = "0.25.1698801123";
                  sha256 = "sha256-ceJaj8Hjna3V4ihsOE4CXk+zMD5XWZc42T9J4CrpvR4=";
                }
              ];
            }
          )
        ];
        shellHook =
          checks.pre-commit-check.shellHook +
          ''
            export JAVA_HOME=${pkgs.jdk}/lib/openjdk
          '';
      };
    };
}

