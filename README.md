# flux

## Software

- kubectl
- kind
- k9s
- [age](https://github.com/FiloSottile/age)
- [flux](https://fluxcd.io/flux/installation/)

## Steps

1. Decrypt secrets

```bash
./scripts/secrets.sh d
```

2. Create cluster

```bash
./scripts/cluster.sh c
```

3. Bootstrap cluster

```bash
./scripts/cluster.sh b
```

4. Create Toggle after Unleash-Server is running

```bash
./scripts/toggle.sh create <toggle_name>
```

The following are needed for this Project: "Toggle, Java, Go, Rust" 


4. Activating and Deactivating Toggles

```bash
./scripts/toggle.sh toggleOn <toggle_name>
```

```bash
./scripts/toggle.sh toggleOff <toggle_name>
```

Currently it toggles both Production and Developent. 

Note:

You can also manually toggle via the dashboard at localhost:4242 using the credentials: Username - admin, Password - unleash4all.

Only enabling the "Toggle" as needed is fundamental and sufficient to get the Project running.

5. Delete cluster

```bash
./scripts/cluster.sh d
```
